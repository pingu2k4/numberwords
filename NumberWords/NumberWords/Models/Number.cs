﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace NumberWords.Models
{
    class Number : INotifyPropertyChanged
    {
        public Number()
        {

        }

        public Number(long Num, string Word, int NumLetters, int NumJumps) : this()
        {
            this.Num = Num;
            this.Word = Word;
            this.NumLetters = NumLetters;
            this.NumJumps = NumJumps;
        }

        public Number(long Num, string Word, int NumLetters, int NumJumps, List<int> Jumps) : this(Num, Word, NumLetters, NumJumps)
        {
            this.Jumps = Jumps;
        }
        
        private long _Num;
        public long Num
        {
            get
            {
                return _Num;
            }
            set
            {
                _Num = value;
                OnPropertyChanged("Num");
            }
        }

        private string _Word;
        public string Word
        {
            get
            {
                return _Word;
            }
            set
            {
                _Word = value;
                OnPropertyChanged("Word");
            }
        }

        private int _NumLetters;
        public int NumLetters
        {
            get
            {
                return _NumLetters;
            }
            set
            {
                _NumLetters = value;
                OnPropertyChanged("NumLetters");
            }
        }
        
        private int _NumJumps;
        public int NumJumps
        {
            get
            {
                return _NumJumps;
            }
            set
            {
                _NumJumps = value;
                OnPropertyChanged("NumJumps");
            }
        }
        
        private List<int> _Jumps = new List<int>();
        public List<int> Jumps
        {
            get
            {
                return _Jumps;
            }
            set
            {
                _Jumps = value;
                OnPropertyChanged("Jumps");
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                if (handler.Target is CollectionView)
                {
                    ((CollectionView)handler.Target).Refresh();
                }
                else
                {
                    handler(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        }

        #endregion
    }
}
