﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace NumberWords.Models
{
    class SaveFile : INotifyPropertyChanged
    {
        public SaveFile()
        {

        }
        
        private List<Number> _Numbers = new List<Number>();
        public List<Number> Numbers
        {
            get
            {
                return _Numbers;
            }
            set
            {
                _Numbers = value;
                OnPropertyChanged("Numbers");
            }
        }
        
        private int _MaxJumps;
        public int MaxJumps
        {
            get
            {
                return _MaxJumps;
            }
            set
            {
                _MaxJumps = value;
                OnPropertyChanged("MaxJumps");
            }
        }
        
        private long _MaxJumpsResponsible;
        public long MaxJumpsResponsible
        {
            get
            {
                return _MaxJumpsResponsible;
            }
            set
            {
                _MaxJumpsResponsible = value;
                OnPropertyChanged("MaxJumpsResponsible");
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                if (handler.Target is CollectionView)
                {
                    ((CollectionView)handler.Target).Refresh();
                }
                else
                {
                    handler(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        }

        #endregion
    }
}
