﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using NumberWords.Commands;
using NumberWords.Models;
using NumberWords.Views;
using Newtonsoft.Json;
using System.IO;

namespace NumberWords.ViewModels
{
    class MainWindowViewModel : INotifyPropertyChanged
    {
        public MainWindowViewModel()
        {
            SettingsUpgrade();
            SetBGOpacity();
            PopulateStarterContent();
            CreateDirs();
        }

        #region BOOT
        private void SettingsUpgrade()
        {
            if (Properties.Settings.Default.NeedUpgrade)
            {
                Properties.Settings.Default.Upgrade();
                Properties.Settings.Default.NeedUpgrade = false;

                Properties.Settings.Default.Save();
            }
        }

        private void PopulateStarterContent()
        {
            SF.Numbers.Add(new Number(0, "Zero", 4, 1, new List<int>(new int[] { 4 })));
            SF.Numbers.Add(new Number(1, "One", 3, 3, new List<int>(new int[] { 4, 5, 3 })));
            SF.Numbers.Add(new Number(2, "Two", 3, 3, new List<int>(new int[] { 4, 5, 3 })));
            SF.Numbers.Add(new Number(3, "Three", 5, 2, new List<int>(new int[] { 4, 5 })));
            SF.Numbers.Add(new Number(4, "Four", 4, 0, new List<int>()));
        }

        private void CreateDirs()
        {
            Directory.CreateDirectory("Data");
        }
        #endregion

        #region METHODS
        private void BGW_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            string Out = JsonConvert.SerializeObject(SF);

            using (StreamWriter SW = new StreamWriter(@"Data\Data.dat"))
            {
                SW.Write(Out);
            }
            Status = "Complete!";
        }

        private void BGW_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            long State = (long)e.UserState;
            Status = "Current: " + State;
        }

        private void BGW_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker Worker = sender as BackgroundWorker;

            while (true)
            {
                if (Worker.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }

                long i = SF.Numbers.Max(N => N.Num) + 1;

                string Word = i.ToText();

                int NumLetters = Word.Replace(" ", "").Length;

                Number JumpedTo = SF.Numbers.SingleOrDefault(SFN => SFN.Num == NumLetters);

                int NumJumps = JumpedTo.NumJumps + 1;

                List<int> Jumps = new List<int>(JumpedTo.Jumps);
                Jumps.Add(NumLetters);

                SF.Numbers.Add(new Number(i, Word, NumLetters, NumJumps, Jumps));

                if(NumJumps > SF.MaxJumps)
                {
                    SF.MaxJumps = NumJumps;
                    SF.MaxJumpsResponsible = i;
                }

                if(i % 100 == 0)
                {
                    Worker.ReportProgress(0, i);

                    if(i % 10000 == 0)
                    {
                        using (StreamWriter SW = new StreamWriter(@"Data\Data.dat"))
                        {
                            JsonSerializer Serializer = new JsonSerializer();
                            Serializer.Serialize(SW, SF);
                        }
                    }
                }
            }
        }
        
        private void BGWLoader_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Status = "LOADED!";
        }

        private void BGWLoader_DoWork(object sender, DoWorkEventArgs e)
        {
            JsonSerializer Serializer = new JsonSerializer();

            using (StreamReader SR = new StreamReader(@"Data\Data.dat"))
            using (JsonTextReader JTR = new JsonTextReader(SR))
            {
                SF = Serializer.Deserialize<SaveFile>(JTR);
            }
        }
        #endregion

        #region PROPERTIES
        #region PANELS

        #endregion
        #region APP
        private string _Status;
        public string Status
        {
            get
            {
                return _Status;
            }
            set
            {
                _Status = value;
                OnPropertyChanged("Status");
            }
        }
        
        private SaveFile _SF = new SaveFile();
        public SaveFile SF
        {
            get
            {
                return _SF;
            }
            set
            {
                _SF = value;
                OnPropertyChanged("SF");
            }
        }

        BackgroundWorker BGW = new BackgroundWorker();
        BackgroundWorker BGWLoader = new BackgroundWorker();
        #endregion
        #region SETTINGS
        public string BGImage
        {
            get
            {
                if (Properties.Settings.Default.BackgroundImage == 0)
                {
                    return "Media/Backgrounds/Background" + Properties.Settings.Default.BackgroundImage + ".png";
                }
                return "Media/Backgrounds/Background" + Properties.Settings.Default.BackgroundImage + ".jpg";
            }
            set
            {
                OnPropertyChanged("BGImage");
            }
        }

        private double bgOpacity;
        public double BGOpacity
        {
            get
            {
                return bgOpacity;
            }
            set
            {
                bgOpacity = value;
                OnPropertyChanged("BGOpacity");
            }
        }
        private void SetBGOpacity()
        {
            switch (Properties.Settings.Default.BackgroundImage)
            {
                case 1:
                    BGOpacity = 0.1;
                    break;
                case 2:
                    BGOpacity = 0.1;
                    break;
                case 3:
                    BGOpacity = 0.3;
                    break;
                case 4:
                    BGOpacity = 0.6;
                    break;
                case 5:
                    BGOpacity = 0.2;
                    break;
                case 6:
                    BGOpacity = 0.6;
                    break;
                case 7:
                    BGOpacity = 0.2;
                    break;
                case 8:
                    BGOpacity = 0.2;
                    break;
                case 9:
                    BGOpacity = 0.1;
                    break;
                case 10:
                    BGOpacity = 0.5;
                    break;
                default:
                    BGOpacity = 0.5;
                    break;
            }
        }
        #endregion
        #region MISC
        private ProgramVersion programVersion = new ProgramVersion();
        public string ProgramVersion
        {
            get
            {
                return programVersion.Version;
            }
        }
        #endregion
        #endregion

        #region COMMANDS
        #region APP
        private ICommand _Start;
        public ICommand Start
        {
            get
            {
                if (_Start == null)
                {
                    _Start = new RelayCommand(StartEx, null);
                }
                return _Start;
            }
        }
        private void StartEx(object p)
        {
            BGW.DoWork += BGW_DoWork;
            BGW.ProgressChanged += BGW_ProgressChanged;
            BGW.RunWorkerCompleted += BGW_RunWorkerCompleted;
            BGW.WorkerReportsProgress = true;
            BGW.WorkerSupportsCancellation = true;
            BGW.RunWorkerAsync();
        }

        private ICommand _Stop;
        public ICommand Stop
        {
            get
            {
                if (_Stop == null)
                {
                    _Stop = new RelayCommand(StopEx, null);
                }
                return _Stop;
            }
        }
        private void StopEx(object p)
        {
            BGW.CancelAsync();
        }
        
        private ICommand _Load;
        public ICommand Load
        {
            get
            {
                if (_Load == null)
                {
                    _Load = new RelayCommand(LoadEx, null);
                }
                return _Load;
            }
        }
        private void LoadEx(object p)
        {
            Status = "Loading...";

            BGWLoader.DoWork += BGWLoader_DoWork;
            BGWLoader.RunWorkerCompleted += BGWLoader_RunWorkerCompleted;
            BGWLoader.WorkerReportsProgress = true;
            BGWLoader.WorkerSupportsCancellation = true;
            BGWLoader.RunWorkerAsync();
        }
        #endregion
        #region MISC
        private ICommand openPreferences;
        public ICommand OpenPreferences
        {
            get
            {
                if (openPreferences == null)
                {
                    openPreferences = new RelayCommand(OpenPreferencesEx, null);
                }
                return openPreferences;
            }
        }
        private void OpenPreferencesEx(object p)
        {
            PreferencesWindow PrefWindow = new PreferencesWindow();
            PreferencesWindowViewModel PrefWindowViewModel = new PreferencesWindowViewModel();
            PrefWindow.DataContext = PrefWindowViewModel;
            PrefWindow.Owner = p as MainWindow;
            PrefWindow.ShowDialog();

            if (PrefWindowViewModel.Saved)
            {
                if (PrefWindowViewModel.BG0)
                {
                    Properties.Settings.Default.BackgroundImage = 0;
                }
                if (PrefWindowViewModel.BG1)
                {
                    Properties.Settings.Default.BackgroundImage = 1;
                }
                if (PrefWindowViewModel.BG2)
                {
                    Properties.Settings.Default.BackgroundImage = 2;
                }
                if (PrefWindowViewModel.BG3)
                {
                    Properties.Settings.Default.BackgroundImage = 3;
                }
                if (PrefWindowViewModel.BG4)
                {
                    Properties.Settings.Default.BackgroundImage = 4;
                }
                if (PrefWindowViewModel.BG5)
                {
                    Properties.Settings.Default.BackgroundImage = 5;
                }
                if (PrefWindowViewModel.BG6)
                {
                    Properties.Settings.Default.BackgroundImage = 6;
                }
                if (PrefWindowViewModel.BG7)
                {
                    Properties.Settings.Default.BackgroundImage = 7;
                }
                if (PrefWindowViewModel.BG8)
                {
                    Properties.Settings.Default.BackgroundImage = 8;
                }
                if (PrefWindowViewModel.BG9)
                {
                    Properties.Settings.Default.BackgroundImage = 9;
                }
                if (PrefWindowViewModel.BG10)
                {
                    Properties.Settings.Default.BackgroundImage = 10;
                }
                Properties.Settings.Default.Save();
                OnPropertyChanged("BGImage");
                SetBGOpacity();
            }
        }
        #endregion
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                if (handler.Target is CollectionView)
                {
                    ((CollectionView)handler.Target).Refresh();
                }
                else
                {
                    handler(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        }

        #endregion
    }
}
